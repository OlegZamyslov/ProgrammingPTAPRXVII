package homework5;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;

/* The dictionary files must have a name in the format "language-language.txt" (ru-en.txt)
*  File format with dictionary: word;-;translation
*/
public class App {

    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static String getFileName() {
        String pathTranslate;
        while (true) {
            try {
                System.out.println("Please, input filename to translate.");
                pathTranslate = br.readLine();
                if (pathTranslate.equals("0") || new File(pathTranslate).isFile()) {
                    break;
                }
            } catch (IOException e) {
                System.out.println("Wrong path!");
            }
        }
        return pathTranslate;
    }

    public static void main(String[] args) throws NoSuchFileException {
        String res;
        String pathDict;
        Translator myTranslator;

        System.out.println("Welcome to translator!");
        while (true) {
            try {
                System.out.println("Please, input directory for dictionaries. \n");
                res = br.readLine();
                if (new File(res).isDirectory()) {
                    break;
                }
            } catch (IOException e) {
                System.out.println("Wrong path!");
            }
        }
        pathDict = res;
        myTranslator = new Translator(pathDict.charAt(pathDict.length() - 1) == '\\' ? pathDict : pathDict + "\\");

        System.out.println("Please, choose: 1 - auto search dictionaries, 2 - manual language setting, 0 - exit \n ");
        while (true) {
            try {
                res = br.readLine();
                if ((Integer.valueOf(res) >= 0 && Integer.valueOf(res) <= 2)) {
                    break;
                }
            } catch (NumberFormatException | IOException e) {
                System.out.println("Wrong choice!");
            }
        }
        if (res.equals("1")) {
            if (myTranslator.getDictionariesList().size() == 0) {
                System.out.println("No dictionaries in the directory");
            } else {
                System.out.println("Please, choose translation, 0 - exit \n ");
                ArrayList<String> lang = new ArrayList<>();
                for (String s : myTranslator.getDictionariesList().keySet()) {
                    lang.add(s);
                    System.out.println("" + lang.size() + " -> " + s);
                }
                while (true) {
                    try {
                        res = br.readLine();
                        if (res.equals("0") || (Integer.valueOf(res) > 0 && Integer.valueOf(res) <= lang.size())) {
                            break;
                        }
                    } catch (IOException | NumberFormatException e) {
                        System.out.println("Wrong choice!");
                    }
                }
                if (!res.equals("0")) {
                    myTranslator.translateText(getFileName(), myTranslator.getDictionariesList().get(lang.get(Integer.valueOf(res) - 1)));
                }
            }
        } else if (res.equals("2")) {
            System.out.println("Please, specify the direction of text translation \n For example: EN-RU \n 0 - exit");
            while (true) {
                try {
                    res = br.readLine();
                    if (res.length() > 0) {
                        break;
                    }
                } catch (IOException e) {
                    System.out.println("Wrong choice!");
                }
            }
            String direction = res.toLowerCase();
            if (!res.equals("0")) {
                File fileDict = new File(pathDict + direction + ".txt");
                if (!fileDict.exists()) {
                    throw new NoSuchFileException("Cannot find file " + pathDict + direction + ".txt");
                } else {
                    myTranslator.translateText(getFileName(), myTranslator.getDictionariesList().get(direction));

                }
            }
        }
    }
}