package homework5;


import lombok.Getter;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
public class Translator {
    private Map<String, Map<String, String>> dictionariesList;
    private String[] wordsWithoutTranslation = new String[]{"is", "are", "am"};

    public Translator(String path) {
        readDictFiles(path);
    }

    public void translateText(String path, Map dict) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), "Windows-1251"))) {
            String s;
            StringBuilder sb = new StringBuilder();
            while ((s = br.readLine()) != null) {
                sb.append(s).append("\n");
            }
            Matcher matcher = Pattern.compile("([А-Яа-я\\w]*)([?! ;:.()]*)").matcher(sb.toString());
            StringBuilder sbRes = new StringBuilder();
            while (matcher.find()) {
                if (dict.containsKey(matcher.group(1).toLowerCase())) {
                    String word = (String) dict.get(matcher.group(1).toLowerCase());
                    sbRes.append(Character.isUpperCase(matcher.group(1).charAt(0)) ? Character.toUpperCase(word.charAt(0)) +
                            word.substring(1) : word).append(matcher.group(2));
                } else {
                    sbRes.append(Arrays.asList(wordsWithoutTranslation).contains(matcher.group(1)) ? "" : matcher.group(1))
                            .append(matcher.group(2));
                }
            }
            System.out.println(sbRes.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readDictFiles(String path) {
        dictionariesList = new HashMap<>();
        File myFile = new File(path);
        if (myFile.isDirectory()) {
            File[] files = myFile.listFiles((dir, name) -> name.matches("\\S+-\\S+\\.txt"));
            for (int i = 0; i < files.length; i++) {
                String fileName;
                fileName = files[i].getName().substring(0, files[i].getName().length() - 4);
                try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(files[i]), "Windows-1251"))) {
                    Map<String, String> dictionaryMap = new HashMap<>();
                    String s;
                    while ((s = br.readLine()) != null) {
                        if (s.indexOf(";-;") > 0) {
                            dictionaryMap.put(s.substring(0, s.indexOf(";-;")).trim(), s.substring(s.indexOf(";-;") + 3, s.length()).trim().toLowerCase());
                        }
                    }
                    dictionariesList.put(fileName.toLowerCase(), dictionaryMap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
