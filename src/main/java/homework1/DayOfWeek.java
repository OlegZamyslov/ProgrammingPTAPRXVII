package homework1;

/**
 * Enumeration of days of the week
 * <p>
 * Each index element has an index
 */
public enum DayOfWeek {
    MONDAY(0),
    TUESDAY(1),
    WEDNESDAY(2),
    THURSDAY(3),
    FRIDAY(4),
    SATURDAY(5),
    SUNDAY(6);

    private int indexDayOfWeek;

    public int getIndexDayOfWeek() {
        return indexDayOfWeek;
    }

    DayOfWeek(int index) {
        this.indexDayOfWeek = index;
    }

    /**
     * Method of getting the day of the week by index
     *
     * @param index index of the day of the week
     * @return corresponding day of the week
     */
    public static DayOfWeek valueOf(int index) {
        return DayOfWeek.values()[index];
    }

}
