package homework1;

import lombok.Getter;
import lombok.ToString;

import java.util.Calendar;

/**
 * Class for storing dates
 *
 * @author zamyslov oleg
 */

@Getter
public class Date {

    private final Year year;
    private final Month month;
    private final Day day;
    private final int DAYS_IN_WEEK = 7;
    private final int DAYS_IN_YEAR = 365;
    private final int DAYS_IN_LEAP_YEAR = 366;
    private final int MIN_NUMBER_MONTH = 0;
    private final int MAX_NUMBER_MONTH  = 12;

    public Date(int year, int month, int day) throws Exception {
        this.year = new Year(year);
        this.month = new Month(month);
        this.day = new Day(day);
    }

    @Override
    public String toString() {
        return "(" + year.getYearNumber() + "-" + month.getMonthNumber() + "-" + day.getDayNumber() + ')';
    }

    /**
     * Method for receiving the day of the week
     * <p>
     * Using the Calendar class, we get the day of the week for our date (SUNDAY = 1)
     * Produce an offset for further searching in the DayOfWeek enumeration..
     *
     * @return DayOfWeek enumeration item
     */

    public DayOfWeek getDayOfWeek() {
        Calendar c = Calendar.getInstance();
        c.set(getYear().getYearNumber(), getMonth().getMonthNumber() - 1, getDay().getDayNumber());
        int dow = (DAYS_IN_WEEK - 1) - ((DAYS_IN_WEEK + 1) - c.get(Calendar.DAY_OF_WEEK)) % DAYS_IN_WEEK;
        return DayOfWeek.valueOf(dow);
    }

    /**
     * Method for receiving the day of the year
     * <p>
     * The method of obtaining the day of the year. We go through the previous months,
     * get the number of days of the month,given the leap year in our country or not.
     * For the current month, we get the day number.
     *
     * @return the day of the year
     */
    public int getDayOfYear() {
        int dayOfYear = 0;
        for (int i = 1; i < getMonth().getMonthNumber(); i++) {
            dayOfYear += getMonth().getDays(i, getYear().leapYear);
        }
        dayOfYear += (getDay().getDayNumber() - 1);
        return dayOfYear;
    }

    /**
     * Method for calculating the number of days between two dates.
     * <p>
     * Method of calculating the number of days between two dates. If the dates are one year,
     * calculate the difference between the days of the year for each date.
     * If the dates are from different years, calculate the number of days in years between them,
     * add the number of days before the end of the year of the first date
     * and the number of days from the beginning of the year for the second date.
     *
     * @param date second date to calculate the gap
     * @return number of days between dates
     */
    public int daysBetween(Date date) {
        int daysBetween = 0;
        if (getYear().getYearNumber() == date.getYear().getYearNumber())
            daysBetween = date.getDayOfYear() - getDayOfYear();
        else {
            for (int i = getYear().getYearNumber() + 1; i < date.getYear().getYearNumber(); i++) {
                daysBetween += getYear().isCurrYearLeapYear(i) ? DAYS_IN_LEAP_YEAR : DAYS_IN_YEAR;
            }
            daysBetween += date.getDayOfYear() + (getYear().isLeapYear() ? DAYS_IN_LEAP_YEAR - getDayOfYear() : DAYS_IN_YEAR - getDayOfYear());
        }
        return daysBetween;
    }

    @Getter
    @ToString
    public class Year {
        boolean leapYear;
        int yearNumber;

        /**
         * Method for checking the year for leap
         * <p>
         * Method for checking the year for leap. If the year is a leap, return true, otherwise false
         *
         * @param currYear year number for verification
         * @return leap year sign or not
         */
        boolean isCurrYearLeapYear(int currYear) {
            return (currYear % 4 == 0 && currYear % 100 != 0) | (currYear % 400 == 0);
        }

        private void setLeapYear(boolean leapYear) {
            this.leapYear = leapYear;
        }

        private void setYearNumber(int yearNumber) {
            this.yearNumber = yearNumber;
        }

        Year(int currYear) throws Exception {
            if (currYear > 0) {
                setYearNumber(currYear);
                setLeapYear(isCurrYearLeapYear(currYear));
            } else {
                throw new Exception("Wrong year!");
            }
        }

    }

    @Getter
    @ToString
    public class Month {
        int monthNumber;

        private void setMonthNumber(int monthNumber) {
            this.monthNumber = monthNumber;
        }

        public Month(int monthNumber) throws Exception {

            if (monthNumber <= MAX_NUMBER_MONTH && monthNumber > MIN_NUMBER_MONTH) {
                setMonthNumber(monthNumber);
            } else {
                throw new Exception("Wrong month!");
            }
        }

        /**
         * Method for obtaining full days in a month
         * <p>
         * The method of obtaining full days in a month. For February we take into account whether a leap year.
         *
         * @param monthNumber month number
         * @param leapYear    year leap year
         * @return number of days in a month
         */
        public int getDays(int monthNumber, boolean leapYear) {
            int[] daysInMonths = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
            daysInMonths[1] += leapYear ? 1 : 0;
            return daysInMonths[monthNumber - 1];
        }


    }

    @Getter
    @ToString
    public class Day {
        private int dayNumber;

        private void setDayNumber(int dayNumber) {
            this.dayNumber = dayNumber;
        }

        public Day(int dayNumber) throws Exception {

            if (dayNumber <= getMonth().getDays(getMonth().getMonthNumber(), getYear().leapYear) && dayNumber > 0) {
                setDayNumber(dayNumber);
            } else {
                throw new Exception("Wrong day of the month!");
            }
        }
    }
}
