package homework1;


public class App {
    public static void main(String[] args) throws Exception {
        // Set two dates for an example of working with inner classes
        Date currDate = new Date(2000, 1, 1);
        System.out.println("Date: "+currDate.toString()+".day of the week: "+currDate.getDayOfWeek());
        System.out.println("Date: "+currDate.toString()+".day of year number: "+currDate.getDayOfYear());
        Date nextDate = new Date(2002, 10, 1);
        System.out.println("Number of days between dates"+currDate.toString()+"and"
                +nextDate.toString()+": "+currDate.daysBetween(nextDate));

        // Assigning to anonymous classes
        Car myCar = new Car() {
            public String toString() {
                return "My car";
            }
            @Override
            public boolean equals(Object obj) {   //выводим всегда true
                return true;
            } // Override the method, always return true
        };
        Car notMyCar = new Car() {
            public String toString() {
                return "Your car";
            }
            @Override
            public boolean equals(Object obj) { //выводим всегда true
                return true;
            } //Override the method, always return true
        };

        System.out.println();
        System.out.println("An example of working with anonymous classes, the toString method");
        System.out.println("The overridden toString method for the first class:"+myCar);
        System.out.println("The overridden toString method for the second class:"+notMyCar);


        System.out.println("An example of working with anonymous classes, the equals method");
        System.out.println("The overridden equals method for the first class:"+myCar.equals(notMyCar));
        System.out.println("The overridden equals method for the second class:"+notMyCar.equals(myCar));
    }
}
