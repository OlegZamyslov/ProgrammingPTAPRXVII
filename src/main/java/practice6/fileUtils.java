package practice6;

import java.io.*;


public class fileUtils {
    public static void findFiles(String start, String regex, String log) {
        File file = new File(start);
        File[] files = file.listFiles();
        for (File myFile : files) {
            if (myFile.isFile()) {
                if (myFile.getName().contains(regex)) {
                    writeToFile(myFile.getName(), log);
                }
            } else {
                System.out.println(myFile.getAbsolutePath());
                try {
                    System.out.println(myFile.getCanonicalPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println(myFile.getPath());
                Thread thread = new Thread(() -> findFiles(myFile.getPath(), regex, log));
                thread.start();
            }
        }
    }

    private static void writeToFile(String name, String log) {
        try (BufferedWriter bufferWriter = new BufferedWriter(new FileWriter(log, true))) {
            bufferWriter.newLine();
            bufferWriter.write(name);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void copyFiles(String start, String dest) {
        File file = new File(start);
        File[] files = file.listFiles();
        for (File myFile : files) {
            if (myFile.isFile()) {
                copyToFile(myFile, dest);
            } else {
                File destdir = new File(dest, myFile.getName());
                destdir.mkdirs();
                Thread thread = new Thread(() -> copyFiles(myFile.getPath(), destdir.getPath()));
                thread.start();
            }
        }
    }

    private static void copyToFile(File name, String dest) {
        try (FileOutputStream fo = new FileOutputStream(dest + File.separator + name.getName());
             FileInputStream fs = new FileInputStream(name.getCanonicalPath())) {
            int i;
            while ((i = fs.read()) > 0) {
                fo.write(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
