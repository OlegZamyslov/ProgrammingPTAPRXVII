package practice6;

import java.util.Map;


public class myShedule {
    public static void getMap(Map<Integer,String> myMap) {
        for (Integer time:myMap.keySet()) {
            String message = myMap.get(time);
            int key = time;
            Thread thread = new Thread(()->{
                try {
                    while(true) {
                        System.out.println(message);
                        Thread.sleep(key);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        }
    }
}
