package practice3;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

public interface GenericStorage<K, V> {

    K add(V value);
    V read(K key);
    boolean update(K key, V value);
    V delete(K key);

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    class Node<K, V> {
        K key;
        V value;
    }

}
