package practice3;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class User extends Entity{
    private String login;
    private String password;
    private String name;
    private String surname;
    private Role role;

}
