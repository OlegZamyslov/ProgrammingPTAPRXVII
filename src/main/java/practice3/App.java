package practice3;


public class App {
    public static void main(String[] args) {
        GenericStorage<Integer, User> userStorage = new MyGenericStorage<>();
        userStorage.add(new User("login1", "pass1", "Name1", "Surname1", Role.ADMIN));
        userStorage.add(new User("login2", "pass2", "Name2", "Surname2", Role.USER));
        System.out.println(userStorage.delete(1));
        userStorage.add(new User("login3", "pass3", "Name3", "Surname3", Role.USER));
        System.out.println(userStorage.read(2));
        userStorage.update(2, new User("login4", "pass4", "Name4", "Surname4", Role.USER));
        System.out.println(userStorage.read(2));

        GenericStorage<Integer, Movie> userStorageMovie = new MyGenericStorage<>();
        userStorageMovie.add(new Movie("Movie1", 2017));
        userStorageMovie.add(new Movie("Movie2", 2017));
        System.out.println(userStorageMovie.delete(1));
        userStorageMovie.add(new Movie("Movie3", 2017));
        System.out.println(userStorageMovie.read(2));
        userStorageMovie.update(2, new Movie("Movie4", 2017));
        System.out.println(userStorageMovie.read(2));


    }
}
