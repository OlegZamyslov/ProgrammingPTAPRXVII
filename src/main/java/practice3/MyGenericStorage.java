package practice3;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class MyGenericStorage<V extends Entity> implements GenericStorage<Integer, V> {
    private List<Node<Integer, V>> nodeList = new ArrayList<>();
    private Integer id = 0;

    public Integer add(V value) {
        nodeList.add(new Node<>(id, value));
        return id++;
    }

    public V read(Integer key) {
        for (Node<Integer, V> nodeelem : nodeList) {
            if (nodeelem.getKey().equals(key)) {
                return nodeelem.getValue();
            }
        }
        return null;
    }

    public boolean update(Integer key, V value) {
        for (Node<Integer, V> nodeelem : nodeList) {
            if (nodeelem.getKey().equals(key)) {
                nodeelem.setValue(value);
                return true;
            }
        }
        return false;
    }

    public V delete(Integer key) {
        for (Node<Integer, V> nodeelem : nodeList) {
            if (nodeelem.getKey().equals(key)) {
                nodeList.remove(nodeelem);
                return nodeelem.getValue();
            }
        }
        return null;
    }
}
