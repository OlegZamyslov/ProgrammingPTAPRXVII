package practice3;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Ticket extends Entity{
    private Movie movie;
    private User user;
    private Session session;
    private int place;

}
