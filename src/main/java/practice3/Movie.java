package practice3;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class Movie extends Entity{
    private String name;
    private int year;
}
