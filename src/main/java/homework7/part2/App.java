package homework7.part2;


public class App {
    public static void main(String[] args) {
        Deadlock d1 = new Deadlock();
        Deadlock d2 = new Deadlock();
        new Thread(()->d1.method01(d2)).start();
        new Thread(()->d2.method01(d1)).start();

    }
}