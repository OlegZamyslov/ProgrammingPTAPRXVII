package homework7.part2;


public class Deadlock {
    public synchronized void method01(Deadlock d) {
        d.method02(this);
    }

    public synchronized void method02(Deadlock d) {
        d.method01(this);
    }


}
