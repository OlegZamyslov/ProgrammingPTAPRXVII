package homework7.part1;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

public class App {
    public static void main(String[] args) {
        myThread thread = new myThread();
        Thread thread2 = new Thread(() -> {
            while (true) {
                System.out.println("time #2:  " + new SimpleDateFormat("HH:mm:ss").format(new Date()));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    return;
                }
            }
        });
        thread.start();
        thread2.start();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            if (br.read() > 0) {
                thread.interrupt();
                thread2.interrupt();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
