package homework7.part1;

import java.text.SimpleDateFormat;
import java.util.Date;


public class myThread extends Thread {
    @Override
    public void run() {
        while (true) {
            try {
                System.out.println("time #1:  " + new SimpleDateFormat("HH:mm:ss").format(new Date()));
                sleep(1000);
            } catch (InterruptedException e) {
                return;
            }
        }
    }
}
