package homework7.part3;


public class CounterThread implements Runnable {

    public void increment1() {
        Counter.counter1++;
    }

    public void increment2() {
        Counter.counter2++;
    }

    public void compare() {
        if (Counter.counter1 == Counter.counter2) {
            System.out.println("" + Counter.counter1 + " = " + Counter.counter2);
        } else if (Counter.counter1 > Counter.counter2) {
            System.out.println("" + Counter.counter1 + " > " + Counter.counter2);
        } else {
            System.out.println("" + Counter.counter1 + " < " + Counter.counter2);
        }
    }


    @Override
    public void run() {
        synchronized (Counter.class) {
            while (true) {
                compare();
                increment1();
                try {
                    Thread.currentThread().sleep(10);
                } catch (InterruptedException e) {
                    return;
                }
                increment2();

            }
        }
    }
}
