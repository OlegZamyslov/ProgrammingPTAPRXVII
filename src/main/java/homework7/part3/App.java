package homework7.part3;


public class App {

    public static void main(String[] args) {
        CounterThread ct = new CounterThread();
        Thread thread1 = new Thread(ct);
        Thread thread2 = new Thread(ct);
        Thread thread3 = new Thread(ct);
        thread1.start();
        thread2.start();
        thread3.start();


        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread1.interrupt();
        thread2.interrupt();
        thread3.interrupt();
    }

}
