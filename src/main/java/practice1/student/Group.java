package practice1.student;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Group {
    int course;
    String faculty;

    public Group(int course, String faculty) {
        setCourse(course);
        setFaculty(faculty);
    }
}
