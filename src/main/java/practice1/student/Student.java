package practice1.student;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class Student {
    String name;
    String surName;
    Group group;
    ArrayList<Exam> listExam;

    public Student(String name, String surName, Group group) {
        setName(name);
        setSurName(surName);
        setGroup(group);
        setListExam(new ArrayList<>());
    }

    public void addExam(Exam exam) {
        getListExam().add(exam);
    }

    public boolean addMark(int mark, ExamNames examName, int semester, int year) {
        for (Exam exam : listExam) {
            if (exam.getName() == examName && exam.getYear() == year && exam.getSemester() == semester) {
                exam.setMark(mark);
                return true;
            }
        }
        return false;
    }

    public boolean deleteMark(ExamNames examName, int semester, int year) throws NoSuchExamException {
        for (Exam exam : listExam) {
            if (exam.getName() == examName && exam.getYear() == year && exam.getSemester() == semester) {
                exam.setMark(0);
                return true;
            }
        }
        throw new NoSuchExamException();
    }

    /* The number of exams that he passed with the indicated mark*/
    public int numberExam(int mark) {
        int q = 0;
        for (Exam exam : listExam) {
            if (exam.getMark() == mark) {
                q++;
            }
        }
        return q;
    }

    /* Average mark for the semester*/
    public double averageMark(int semester, int year) {
        int quantity = 0;
        int mark = 0;
        for (Exam exam : listExam) {
            if (exam.getYear() == year && exam.getSemester() == semester) {
                quantity++;
                mark += exam.getMark();
            }
        }
        if (quantity == 0) {
            return 0;
        } else {
            return (double)mark / quantity;
        }
    }

}
