package practice1.student;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Exam {
    ExamNames name;
    int mark;
    int semester;
    int year;

    public Exam(ExamNames name, int mark, int semester, int year) {
        setName(name);
        setMark(mark);
        setSemester(semester);
        setYear(year);
    }
}
