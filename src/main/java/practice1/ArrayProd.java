package practice1;

public class ArrayProd {
    int[] ClassArray;

    public int prod() {
        int result = 1;
        for (int i = 0; i < ClassArray.length; i++) {
            result *= ClassArray[i];
        }
        return result;
    }


    public ArrayProd(int[] array) {
        this.ClassArray = array;
    }

    public static int prod(int[] array) {
        int result = 1;
        for (int i = 0; i < array.length; i++) {
            result *= array[i];
        }
        return result;
    }
}
