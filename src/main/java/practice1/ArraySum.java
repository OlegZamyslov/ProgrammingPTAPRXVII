package practice1;

public class ArraySum {

    int[] ClassArray;

    public int sum() {
        int result = 0;
        for (int i = 0; i <ClassArray.length ; i++) {
            result +=ClassArray[i];
        }
        return result;
    }

    public ArraySum(int[] array) {
        this.ClassArray = array;
    }

    public static int sum(int[] array) {
        int result = 0;
        for (int i = 0; i <array.length ; i++) {
            result +=array[i];
        }
        return result;
    }
}
