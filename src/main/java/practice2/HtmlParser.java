package practice2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class HtmlParser {
    public String readTextFromFile(String path,String charSet) {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(App.class.getClassLoader().getResourceAsStream(path),charSet))) {
            String s;
            while ((s= br.readLine()) !=null) {
                sb.append(s).append("\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }


    public List<Position> parseBaby(String path,String charSet) {
        String s = readTextFromFile(path,charSet);
        List<Position> list = new ArrayList();
        Pattern pattern = Pattern.compile("<td>(?<pos>.+)</td><td>(?<male>.+)</td><td>(?<female>.+)</td>");
        Matcher matcher = pattern.matcher(s);
        while (matcher.find()) {
            list.add(new Position(Integer.valueOf(matcher.group("pos")),matcher.group("male"),matcher.group("female")));
        }
        return list;
    }

    public List<Comp> parseComp(String path,String charSet) {
        String s = readTextFromFile(path,charSet);
        List<Comp> list = new ArrayList();
        Pattern pattern = Pattern.compile("box\" title=\"(?<name>.+)\" class=\"image\">.+class=\"description\">(?<descr>.+)<br />([\\s\\S]+?)class=\"price cost\">(?<price>.+) грн</span>");
        Matcher matcher = pattern.matcher(s);
        while (matcher.find()) {
            list.add(new Comp(matcher.group("name"),matcher.group("descr"),Integer.valueOf(matcher.group("price"))));
        }
        return list;
    }

}
