package practice2;


public class Position {
    int number;
    String male;
    String female;

    public Position(int number, String male, String female) {
        this.number = number;
        this.male = male;
        this.female = female;
    }

    @Override
    public String toString() {
        return "{" +
                "number=" + number +
                ", '" + male + '\'' +
                ", '" + female + '\'' +
                '}';
    }
}
