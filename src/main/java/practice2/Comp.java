package practice2;


public class Comp {
    String name;
    String descr;
    int price;

    public Comp(String name, String descr, int price) {
        this.name = name;
        this.descr = descr;
        this.price = price;
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", descr='" + descr + '\'' +
                ", price=" + price +
                '}';
    }
}
