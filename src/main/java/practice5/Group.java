package practice5;

import lombok.Data;

import java.util.List;


@Data
public class Group {
    List<Student> listStudent;

    public Group(List<Student> listStudent) {
        this.listStudent = listStudent;
    }
}
