package practice5;

import com.google.gson.Gson;
import homework2.StringUtils;
import lombok.Data;
import practice4.IOUtils;

import java.io.*;
import java.util.*;
import java.util.stream.IntStream;


@Data
public class MyIOUtils {

    public static void saveFile(String path, List<Integer> list) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path))) {
            for (Integer number : list) {
                bw.write(number + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static void fillFile(String path, int quantity) {
        List<Integer> list = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < quantity; i++) {
            list.add(random.nextInt(100));
        }
        saveFile(path, list);
    }

    public static void sortFile(String path, String path2) {
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            List<Integer> listInt = new ArrayList<>();
            String s;
            while ((s = br.readLine()) != null) {
                listInt.add(Integer.valueOf(s));
            }
            listInt.sort((s1, s2) -> (s1 - s2));
            saveFile(path2, listInt);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void averageMark(String path, int mark) {
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            Map<String, ArrayList<Integer>> mapStudent = new HashMap<>();
            String s;
            while ((s = br.readLine()) != null) {
                if (s.indexOf("=") > 0) {
                    String key = s.substring(0, s.indexOf("=") - 1);
                    int value = Integer.valueOf(s.substring(s.indexOf("=") + 2, s.length()).trim());
                    if (mapStudent.containsKey(key)) {
                        mapStudent.get(key).add(value);
                    } else {
                        ArrayList list = new ArrayList();
                        list.add(value);
                        mapStudent.put(key, list);
                    }
                }
            }
            for (Map.Entry<String, ArrayList<Integer>> entry : mapStudent.entrySet()) {
                String key = entry.getKey();
                ArrayList<Integer> value = entry.getValue();
                int sum = 0;
                for (int i = 0; i < value.size(); i++) {
                    sum += value.get(i);
                }
                int average = sum / value.size();
                if (average > mark) {
                    System.out.println(key);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void changeWords(String path, String pathdest) {
        try (BufferedReader br = new BufferedReader(new FileReader(path));
             BufferedWriter bw = new BufferedWriter(new FileWriter(pathdest))) {
            String s;
            while ((s = br.readLine()) != null) {
                String changedWords = StringUtils.changeFirstLastWordEverywhere(s);
                bw.write(changedWords+"\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void BufferedSymb(String path, String pathdest) {
        try (BufferedReader br = new BufferedReader(new FileReader(path));
             BufferedWriter bw = new BufferedWriter(new FileWriter(pathdest))) {
            String s;
            while ((s = br.readLine()) != null) {
                bw.write(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void Symb(String path, String pathdest) {
        try (FileReader br = new FileReader(path);
             FileWriter bw = new FileWriter(pathdest)) {
            int s;
            while ((s = br.read()) != -1) {
                bw.write(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void BufferedBit(String path, String pathdest) {
        try (BufferedInputStream bi = new BufferedInputStream(new FileInputStream(path));
             BufferedOutputStream bo = new BufferedOutputStream(new FileOutputStream(pathdest))) {
            int s;
            while ((s = bi.read()) != -1) {
                bo.write(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void Bit(String path, String pathdest) {
        try (FileInputStream bi = new FileInputStream(path);
             FileOutputStream bo = new FileOutputStream(pathdest)) {
            int s;
            while ((s = bi.read()) != -1) {
                bo.write(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void serialize(Group group, String path) {
        Gson gson = new Gson();
        String s = gson.toJson(group);
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path))) {
            bw.write(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Group deserialize(String path) {
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String s;
            s = br.readLine();
            Gson gson = new Gson();
            Group group = gson.fromJson(s, Group.class);
            return group;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
