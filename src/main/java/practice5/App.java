package practice5;

import java.util.ArrayList;
import java.util.List;


public class App {

    public static void main(String[] args) {
        MyIOUtils myFile = new MyIOUtils();
        /* Item 1 of the assignment */
        myFile.fillFile("src/resources/example.txt", 10);
        /* Item 2 of the assignment */
        myFile.sortFile("src/resources/example.txt", "src/resources/examplesorted.txt");
        /* Item 3 of the assignment */
        myFile.averageMark("src/resources/marks.txt", 90);
        /* Item 4 of the assignment */
        myFile.changeWords("src/resources/changeWord.txt", "src/resources/changeWord2.txt");

        /* Item 5 of the assignment */
        for (int i = 0; i < 100; i++) {
            myFile.BufferedBit("src/resources/marks.txt", "src/resources/markscopy.txt");
            myFile.Bit("src/resources/marks.txt", "src/resources/markscopy.txt");
            myFile.BufferedSymb("src/resources/marks.txt", "src/resources/markscopy.txt");
            myFile.Symb("src/resources/marks.txt", "src/resources/markscopy.txt");
        }
        long start = System.currentTimeMillis();
        myFile.BufferedBit("src/resources/marks.txt", "src/resources/markscopy.txt");
        long end = System.currentTimeMillis();
        System.out.println("BufferedBit:" + (end - start));
        start = System.currentTimeMillis();
        myFile.BufferedSymb("src/resources/marks.txt", "src/resources/markscopy.txt");
        end = System.currentTimeMillis();
        System.out.println("BufferedSymb:" + (end - start));
        start = System.currentTimeMillis();
        myFile.Symb("src/resources/marks.txt", "src/resources/markscopy.txt");
        end = System.currentTimeMillis();
        System.out.println("Symb:" + (end - start));
        start = System.currentTimeMillis();
        myFile.Bit("src/resources/marks.txt", "src/resources/markscopy.txt");
        end = System.currentTimeMillis();
        System.out.println("Bit:" + (end - start));

        /* Item 6 of the assignment */
        List<Student> list = new ArrayList<>();
        list.add(new Student("Vasya", 25));
        list.add(new Student("Petya", 20));
        list.add(new Student("Olga", 27));
        Group group = new Group(list);
        myFile.serialize(group, "src/resources/serial.txt");
        Group group2;
        group2 = myFile.deserialize("src/resources/serial.txt");
        System.out.println(group2.getListStudent());
    }

}
