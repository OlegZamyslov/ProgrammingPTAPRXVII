package homework3;


import java.util.Arrays;

public class GenericStorage<T> {
    private T[] array;
    private int arraySize = 0;


    public GenericStorage() {
        array = (T[]) new Object[10];
    }

    public GenericStorage(int size) {
        if (size > 0) {
            array = (T[]) new Object[size];
        } else {
            throw new IllegalArgumentException("Wrong size!");
        }
    }

    /* Method, which adds a new item to the repository at the end */
    public boolean add(T obj) {
        if (obj == null) {
            throw new NullPointerException();
        }
        if (arraySize < array.length) {
            array[arraySize] = obj;
            arraySize++;
            return true;
        } else {
            return false;
        }
    }

    /* Method, which returns an element by index in an array. */
    public T get(int index) {
        if (index < arraySize && index >= 0) {
            return array[index];
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    /* Method, which returns an array of elements. */
    public T[] getAll() {
        T[] resArray = (T[]) new Object[arraySize];
        System.arraycopy(array, 0, resArray, 0, arraySize);
        System.out.println(Arrays.toString(array));
        return resArray;
    }

    /*
     *Method, which will override the object at the specified position only
     *if there is already an element on this position.
     */
    public boolean update(int index, T obj) {
        if (index < arraySize && index >= 0) {
            array[index] = obj;
            return true;
        } else {
            return false;
        }
    }

    /*
     * Method, which removes the element from the index and closes the empty cell in the array,
     * if there is already an element on this position.
     */
    public boolean delete(int index) {
        if (index < arraySize && index >= 0) {
            for (int i = index; i < arraySize; i++) {
                array[i] = array[i + 1];
            }
            array[arraySize] = null;
            arraySize--;
            return true;
        } else {
            return false;
        }
    }

    /* Method, which removes the specified object from an array. */
    public boolean delete(T obj) {
        int index = Arrays.asList(array).indexOf(obj);
        if (index >= 0) {
            return delete(index);
        } else {
            return false;
        }

    }

    /* Method, which returns the size of the FULL array. */
    public int size() {
        return arraySize;
    }

}
