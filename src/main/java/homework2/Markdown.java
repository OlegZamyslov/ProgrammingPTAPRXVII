package homework2;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* Markdown parser */
public class Markdown {

    public static String parser(String baseString) {
        String[] strings = baseString.split("\n");
        StringBuilder sb = new StringBuilder();
        sb.append("<html>\n<body>\n");
        for (String s : strings) {
            if (s != null) {
                Pattern pattern = Pattern.compile("(#+)(.+)");
                Matcher matcher = pattern.matcher(s);
                if (matcher.find()) {
                    sb.append(s.replaceAll(matcher.group(0), String.format("<h%1$d>%2$s</h%1$d>",
                            matcher.group(1).length(), matcher.group(2))))
                            .append("\n");
                } else {
                    sb.append("<p>");
                    sb.append(s.replaceAll("\\*{2}(\\w+)\\*{2}", "<strong>$1</strong>")
                            .replaceAll("w?\\*(\\w+)\\*w?", "<em>$1</em>")
                            .replaceAll("\\[(.+)]\\((.+)\\)", "<a href=“$2“>$1</a>"))
                            .append("</p>").append("\n");
                }
            }
        }
        sb.append("</body>\n</html>\n");
        return sb.toString();
    }
}




