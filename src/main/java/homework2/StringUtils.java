package homework2;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    /* Function that reverses the line in reverse */
    public static String reverseString(String baseString) {
        StringBuilder sb = new StringBuilder(baseString);
        return sb.reverse().toString();
    }

    /* A function that determines whether a string is a palindrome */
    public static boolean isPalindromeString(String baseString) {
        char[] charString = baseString.toLowerCase().replaceAll("[^Wа-я]", "").toCharArray();
        for (int i = 0, j = charString.length - 1; i < charString.length / 2; i++, j--) {
            if (charString[i] != charString[j]) {
                return false;
            }
        }
        return true;
    }

    /*
     * A function that checks the length of a string, and if its length is greater than 10,
     * then leave only the first 6 characters in the string,
     * otherwise add a string with the characters 'o' to length 12
     */
    public static String checkStringLength(String baseString) {
        StringBuilder sb = new StringBuilder(baseString);
        if (sb.length() > 10) return sb.substring(0, 6);
        else {
            for (int i = sb.length(); i < 12; i++) {
                sb.append('o');
            }
            return sb.toString();
        }
    }

    /* Function that swaps the first and last word in a line */
    public static String changeFirstLastWord(String baseString) {
        return baseString.replaceAll("([\\wа-яА-Я]+)([^.]+?)([\\wа-яА-Я]+)([.!?\n])", "$3$2$1$4");
    }

    /*
     * A function that swaps the first and last word in each sentence.
     *(Sentences can be separated ONLY by the sign of the point)
     */
    public static String changeFirstLastWordEverywhere(String baseString) {
        StringBuilder sb = new StringBuilder();
        Pattern pattern = Pattern.compile("([\\wа-яА-Я]+)([^.]+?)([\\wа-яА-Я]+)([.!?\n])");
        Matcher matcher = pattern.matcher(baseString);
        while (matcher.find()) {
            sb.append(matcher.group(3)).append(matcher.group(2))
                    .append(matcher.group(1)).append(matcher.group(4));
        }
        return sb.toString();

    }

    /* Function that checks if the string contains only the characters 'a', 'b', 'c' or not. */
    public static boolean containsABC(String baseString) {
        return Pattern.compile("[abc]+").matcher(baseString).matches();
    }

    /* Function that determines whether the string is a date of the format MM.DD.YYYY */
    public static boolean containsDate(String baseString) {
        return Pattern.compile("\\d{2}\\.\\d{2}\\.\\d{4}").matcher(baseString).matches();
    }

    /* A function that determines whether the string is an email address */
    public static boolean containsEmail(String baseString) {
        return Pattern.compile("\\w+([.-]?\\w+)*@\\w+([.-]?\\w+)*\\.\\w{2,4}").matcher(baseString).matches();
    }

    /*
     * Function that finds all phones in the transmitted text of the format + X (XXX) XXX-XX-XX,
     * and returns them as an array
     */
    public static List<String> getPhoneNumbers(String baseString) {
        List<String> listNumbers = new ArrayList<>();
        Pattern pattern = Pattern.compile("\\+\\d\\(\\d{3}\\)\\d{3}-\\d{2}-\\d{2}\\b");
        Matcher matcher = pattern.matcher(baseString);
        while (matcher.find()) {
            listNumbers.add(matcher.group());
        }
        return listNumbers;
    }


}
