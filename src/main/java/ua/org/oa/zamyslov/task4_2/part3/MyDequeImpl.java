package ua.org.oa.zamyslov.task4_2.part3;


import lombok.Getter;

import java.util.Iterator;
import java.util.NoSuchElementException;

@Getter
public class MyDequeImpl<E> implements MyDeque<E> {
    private int size;
    private Node<E> first;
    private Node<E> last;
    private Node<E> lastRet;
    private boolean canRemove;

    @Getter
    public static class Node<E> {
        private E element;
        private Node<E> next;
        private Node<E> prev;

        public Node(E element, Node<E> next, Node<E> prev) {
            this.element = element;
            this.next = next;
            this.prev = prev;
        }
    }

    public ListIterator<E> listIterator() {
        return new ListIteratorImpl();
    }

    private class ListIteratorImpl extends IteratorImpl implements ListIterator<E> {

        @Override
        public boolean hasPrevious() {
            return cursor > 0;
        }

        @Override
        public E previous() {
            if (cursor >= 0) {
                if (cursor == 0) {
                    lastRet = first;
                } else {
                    if (cursor == size()) {
                        lastRet = last;
                    } else {
                        lastRet = lastRet.getPrev();
                    }
                }
                cursor--;
                canRemove = true;
                return lastRet.element;
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        /*Replaces the element that was returned next / previous in the previous step to this element*/
        public void set(E e) {
            if (!canRemove) {
                throw new IllegalStateException();
            } else {
                lastRet.element = e;
                canRemove = false;
            }
        }
    }

    private class IteratorImpl implements Iterator<E> {
        public int cursor;

        @Override
        public boolean hasNext() {
            return cursor != size;
        }

        @Override
        public E next() {
            if (cursor <= size) {
                if (cursor == 0) {
                    lastRet = first;
                } else {
                    lastRet = lastRet.getNext();
                }
                cursor++;
                canRemove = true;
                return lastRet.element;
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        /*Removes the item that was returned earlier by the next method*/
        public void remove() {
            if (!canRemove) {
                throw new IllegalStateException();
            } else {
                if (lastRet.prev != null) {
                    lastRet.prev.next = lastRet.next;
                }
                if (lastRet.next != null) {
                    lastRet.next.prev = lastRet.prev;
                }
                if (lastRet == first) {
                    first = lastRet.getNext();
                }
                if (lastRet == last) {
                    last = lastRet.getPrev();
                }
                cursor--;
                size--;
                lastRet = lastRet.getPrev();
                canRemove = false;
            }
        }
    }

    public Iterator<E> iterator() {
        return new IteratorImpl();
    }

    @Override
    /* Add an item to the top of the list */
    public void addFirst(E e) {
        if (size != 0) {
            Node<E> prevFirst = first;
            first = new Node<>(e, prevFirst, null);
            prevFirst.prev = first;
        } else {
            last = first = new Node<>(e, null, null);
        }
        size++;
    }

    @Override
    /* Add item to the end of the list */
    public void addLast(E e) {
        if (size != 0) {
            Node<E> prevLast = first;
            for (int i = 0; i < size - 1; i++) {
                prevLast = prevLast.getNext();
            }
            last = new Node<>(e, null, prevLast);
            prevLast.next = last;
        } else {
            last = first = new Node<>(e, null, null);
        }
        size++;
    }

    @Override
    /* Get the item from the beginning of the list and delete it */
    public E removeFirst() {
        if (first != null) {
            if (first == last) {
                last = null;
            }
            E elem = first.getElement();
            first = first.getNext();
            if (first != null) {
                first.prev = null;
            }
            size--;
            return elem;
        } else {
            throw new NoSuchElementException("List is empty");
        }
    }

    @Override
     /* Get the item from the end of the list and delete it */
    public E removeLast() {
        if (last != null) {
            E elem = last.getElement();
            if (first == last) {
                first = null;
            }
            if (last.getPrev() != null) {
                last.prev.next = null;
                last = last.getPrev();
            }
            size--;
            return elem;
        } else {
            throw new NoSuchElementException("List is empty");
        }
    }

    @Override
    /*Get the item from the beginning of the list without deleting it*/
    public E getFirst() {
        if (first != null) {
            return first.getElement();
        } else {
            throw new NoSuchElementException("List is empty");
        }
    }

    @Override
     /*Get the item from the end of the list without deleting it*/
    public E getLast() {
        if (last != null) {
            return last.getElement();
        } else {
            throw new NoSuchElementException("List is empty");
        }
    }

    @Override
    /*Check if the object is in the list*/
    public boolean contains(Object o) {
        for (Node<E> nodeElement = first; nodeElement != null; nodeElement = nodeElement.getNext()) {
            if (nodeElement.getElement().equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    /*clear the list*/
    public void clear() {
        for (Node<E> nodeElement = first; nodeElement != null; nodeElement = nodeElement.getNext()) {
            removeFirst();
        }
    }

    @Override
     /*Return an array of items from the list (preserving the ordering of the list items)*/
    public Object[] toArray() {
        Object[] resultArray = new Object[size()];
        Node<E> elem = first;
        for (int i = 0; i < size; i++) {
            resultArray[i] = elem.getElement();
            elem = elem.next;
        }
        return resultArray;
    }

    @Override
    /*Return the number of items in the list*/
    public int size() {
        return size;
    }

    @Override
    /*Check if the list contains all items in the deque list*/
    public boolean containsAll(MyDeque<? extends E> deque) {
        Object[] dequeArray = deque.toArray();
        for (Object dequeArrayElement : dequeArray) {
            if (!contains(dequeArrayElement)) {
                return false;
            }
        }
        return true;

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        for (Node<E> nodeElement = first; nodeElement != null; nodeElement = nodeElement.getNext()) {
            sb.append(nodeElement.getElement().toString()).append(",");
        }
        if (sb.charAt(sb.length() - 1) == ',') {
            return sb.replace(sb.length() - 1, sb.length(), "}").toString();
        } else {
            return sb.append("}").toString();
        }
    }
}
