package ua.org.oa.zamyslov.task4_2.part1;


public class Demo {
    public static void main(String[] args) {
        System.out.println("-----------Number--------------");
        MyDeque<Number> dequeNumber = new MyDequeImpl<>();
        dequeNumber.addFirst(433);
        dequeNumber.addLast(8.88);
        dequeNumber.addFirst(15);
        dequeNumber.addLast(2);
        dequeNumber.addLast(2.48);
        MyDeque<Number> dequeNumber2 = new MyDequeImpl<>();
        dequeNumber2.addLast(433);
        dequeNumber2.addLast(2);
        System.out.println("list contains 433 --> " + dequeNumber.contains(433));
        System.out.println("remove first --> " + dequeNumber.removeFirst());
        System.out.println("remove last --> " + dequeNumber.removeLast());
        System.out.println("get first --> " + dequeNumber.getFirst());
        System.out.println("get last --> " + dequeNumber.getLast());
        System.out.println("to array --> " + dequeNumber.toArray());
        System.out.println("size --> " + dequeNumber.size());
        System.out.println("contains all --> " + dequeNumber.containsAll(dequeNumber2));
        System.out.println("to String --> " + dequeNumber.toString());
        System.out.println("-----------String--------------");
        MyDeque<String> dequeString = new MyDequeImpl<>();
        dequeString.addFirst("Kharkiv");
        dequeString.addLast("Kyiv");
        dequeString.addFirst("Lviv");
        dequeString.addLast("Ternopol");
        dequeString.addLast("Dnepr");
        MyDeque<String> dequeString2 = new MyDequeImpl<>();
        dequeString2.addLast("Ternopol");
        dequeString2.addLast("Kharkiv");
        System.out.println("list contains Kharkiv --> " + dequeString.contains("Kharkiv"));
        System.out.println("remove first --> " + dequeString.removeFirst());
        System.out.println("remove last --> " + dequeString.removeLast());
        System.out.println("get first --> " + dequeString.getFirst());
        System.out.println("get last --> " + dequeString.getLast());
        System.out.println("to array --> " + dequeString.toArray());
        System.out.println("size --> " + dequeString.size());
        System.out.println("contains all --> " + dequeString.containsAll(dequeString2));
        System.out.println("to String --> " + dequeString.toString());
    }

}
