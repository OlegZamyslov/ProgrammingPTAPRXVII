package ua.org.oa.zamyslov.task4_2.part3;


public class Demo {
    public static void main(String[] args) {
        MyDeque<Number> deque = new MyDequeImpl<>();
        deque.addFirst(433);
        deque.addLast(8.88);
        deque.addLast(5.55);
        deque.addLast(3.33);

        for (Number element : deque) {
            System.out.println(element);
        }

        ListIterator<Number> listIt = deque.listIterator();
        while (listIt.hasNext()) {
            System.out.println(listIt.next());
        }
        while (listIt.hasPrevious()) {
            System.out.println(listIt.previous());
            listIt.set(3);
        }
        while (listIt.hasNext()) {
            System.out.println(listIt.next());
        }
    }
}

