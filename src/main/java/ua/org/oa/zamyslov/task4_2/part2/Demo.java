package ua.org.oa.zamyslov.task4_2.part2;


import java.util.Iterator;

public class Demo {
    public static void main(String[] args) {
        MyDeque<Number> deque = new MyDequeImpl<>();
        deque.addFirst(433);
        deque.addLast(8.88);
        deque.addLast(5.55);
        deque.addLast(3.33);

        for (Number element : deque) {
            System.out.println(element);
        }

        Iterator<Number> it = deque.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
            it.remove();
        }
        System.out.println(deque);

    }
}
