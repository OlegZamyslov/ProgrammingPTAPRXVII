package ua.org.oa.zamyslov.task4_2.part3;


public interface ListIterable<E> {
    ListIterator<E> listIterator();
}
