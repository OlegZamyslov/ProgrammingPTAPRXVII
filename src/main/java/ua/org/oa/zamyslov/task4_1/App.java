package ua.org.oa.zamyslov.task4_1;

public class App {

    /* Create a static method to determine the maximum (by compareTo ()) value in an array of objects.*/
    public static <T extends Comparable<T>> T getMaxValue(T[] array) {
        T max = array[0];
        for (T elem : array) {
            if (elem != null && max.compareTo(elem) < 0) {
                max = elem;
            }
        }
        if (max != null) {
            return max;
        } else {
            throw new NullPointerException("Array is empty");
        }
    }

    public static void main(String[] args) {
        /*Demonstrate the work of the method getMaxValue() on the example of arrays of type Integer */
        Integer[] arrayInteger = new Integer[]{1, 5, 8, 4, 6};
        System.out.println(App.getMaxValue(arrayInteger));

        /*Demonstrate the work of the method getMaxValue() on the example of arrays of type String */
        String[] arrayString = new String[]{"Olya", "Vasya", "Petya"};
        System.out.println(App.getMaxValue(arrayString));

        /*Demonstrate the work of the method getMaxValue() on the example of arrays of type Computer */
        Computer[] arrayComputer = new Computer[]{new Computer("My comp1", 2000),
                new Computer("My comp1", 3000), new Computer("My comp1", 2500)};
        System.out.println(App.getMaxValue(arrayComputer));

        /*Demonstrate the work of the method getMaxValue() on the example of arrays of type Car */
        Car[] arrayCar = new Car[]{new Car("My car1", "AA1"),
                new Car("My car2", "FF1"), new Car("My car3", "AA2")};
        /* Compilation error */
        //System.out.println(App.getMaxValue(arrayCar));
    }
}
