package ua.org.oa.zamyslov.task4_1;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Car {
    private String mark;
    private String number;
}
