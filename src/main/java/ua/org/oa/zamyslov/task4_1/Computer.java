package ua.org.oa.zamyslov.task4_1;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class Computer implements Comparable<Computer>{
    private String name;
    private int price;


    @Override
    public int compareTo(Computer o) {
        return price - o.getPrice();
    }
}
