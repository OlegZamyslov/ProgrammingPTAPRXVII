package practice4;


import java.util.*;

public class StudentUtils {
    static Map<String, Student> createMapFromList(List<Student> students) {
        Map<String, Student> studentsMap = new HashMap<>();
        for (Student student : students) {
            studentsMap.put(student.getFirstName() + student.getLastName(), student);
        }
        return studentsMap;
    }

    static void printStudents(List<Student> students, int course) {
        Iterator<Student> iterator = students.iterator();
        while (iterator.hasNext()) {
            Student student = iterator.next();
            if (student.getCourse() == course) {
                System.out.println(student);
            }
        }
    }

    static List<Student> sortStudent(List students) {
        List<Student> resultList = new ArrayList<>(students);
        Collections.sort(resultList, Comparator.comparing(Student::getFirstName));
        return resultList;
    }
}
