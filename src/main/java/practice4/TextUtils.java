package practice4;

import java.util.*;


public class TextUtils {
    static Map<String, Quantity> getWords(String path, String charSet) {
        Map<String, Quantity> mapWords = new HashMap<>();
        String s = IOUtils.readFile(path, charSet);
        String[] words = s.split("[\\s.,;?'\\-!]+");
        for (String word : words) {
            if (mapWords.containsKey(word)) {
                mapWords.get(word).increment();
            } else {
                mapWords.put(word, new Quantity());
            }
        }
        return mapWords;
    }

    static Map<String, Quantity> sortMap(String path, String charSet, SortEnum sort) {
        Map<String, Quantity> mapWords = getWords(path, charSet);
        Map<String, Quantity> sortMap;
        switch (sort) {
            case KEYUP:
                sortMap = new TreeMap<>(Comparator.naturalOrder());
                sortMap.putAll(mapWords);
                break;
            case KEYDOWN:
                sortMap = new TreeMap<>(Comparator.reverseOrder());
                sortMap.putAll(mapWords);
                break;
            case VALUEUP:
                List<Map.Entry<String, Quantity>> list = new LinkedList<>(mapWords.entrySet());
                Collections.sort(list, (o1, o2) -> (o1.getValue().getValue() - o2.getValue().getValue()));
                sortMap = new LinkedHashMap<>();
                for (Map.Entry<String, Quantity> entry : list) {
                    sortMap.put(entry.getKey(), entry.getValue());
                }
                break;
            case VALUEDOWN:
                List<Map.Entry<String, Quantity>> list2 = new LinkedList<>(mapWords.entrySet());
                Collections.sort(list2, (o1, o2) -> (o2.getValue().getValue() - o1.getValue().getValue()));
                sortMap = new LinkedHashMap<>();
                for (Map.Entry<String, Quantity> entry : list2) {
                    sortMap.put(entry.getKey(), entry.getValue());
                }
                break;
            default:
                sortMap = mapWords;
                break;
        }

        return sortMap;

    }

}
