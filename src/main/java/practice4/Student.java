package practice4;

import lombok.Getter;
import lombok.ToString;

/**
 * Created by student on 12.05.2017.
 */
@Getter
public class Student {
    private String firstName;
    private String lastName;
    private int course;

    public Student(String firstName, String lastName, int course) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.course = course;
    }

    @Override
    public String toString() {
        return "Student{" +
                "" + firstName +
                " " + lastName +
                ", course=" + course +
                '}';
    }
}
