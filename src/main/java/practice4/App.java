package practice4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class App {
    public static void main(String[] args) throws IOException{
        List<Student> students = new ArrayList<>();
        students.add(new Student("Petya","Ivanov",2));
        students.add(new Student("Sergey","Ivanov",3));
        students.add(new Student("Olya","Petrova",2));
        students.add(new Student("Masha","Sidorova",1));

        StudentUtils.printStudents(students,2);
        System.out.println("---------------------");
        System.out.println(StudentUtils.sortStudent(students));
        System.out.println("---------------------");
        System.out.println(StudentUtils.createMapFromList(students));
        System.out.println("---------------------");

        Map<String,Quantity> mapWords = TextUtils.getWords("romeo.txt","UTF-8");
        System.out.println(mapWords);

        System.out.println(TextUtils.sortMap("romeo.txt","UTF-8",SortEnum.KEYUP));
        System.out.println(TextUtils.sortMap("romeo.txt","UTF-8",SortEnum.KEYDOWN));
        System.out.println(TextUtils.sortMap("romeo.txt","UTF-8",SortEnum.VALUEUP));
        System.out.println(TextUtils.sortMap("romeo.txt","UTF-8",SortEnum.VALUEDOWN));

    }
}
