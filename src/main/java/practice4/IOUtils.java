package practice4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class IOUtils {
    public static String readFile(String path, String charSet) {
        String s;
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(App.class.getClassLoader().getResourceAsStream(path), charSet))) {
            while ((s = br.readLine()) != null) {
                if (s.length() > 0) {
                    sb.append(s + "\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }
}
