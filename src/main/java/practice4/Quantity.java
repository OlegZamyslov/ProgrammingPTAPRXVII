package practice4;

import lombok.Getter;


@Getter
public class Quantity {
    private int value;

    public Quantity() {
        this.value = 1;
    }

    public void increment() {
        this.value++;
    }

    @Override
    public String toString() {
        return "" + value;
    }
}
