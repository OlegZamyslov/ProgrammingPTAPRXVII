package homework6.part1;


import com.google.gson.Gson;

import java.io.*;
import java.util.ArrayList;

public class bookUtils {
    public static ArrayList<Book> readBookFile(String path) {
        ArrayList<Book> bookArray = new ArrayList<>();
        File myFile = new File(path);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(myFile), "Windows-1251"))) {
            String s;
            while ((s = br.readLine()) != null) {
                String[] array = s.split(";");
                bookArray.add(new Book(array[0], array[1], Integer.valueOf(array[2])));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bookArray;
    }

    public static void serialize(Book book, String path) {
        Gson gson = new Gson();
        String s = gson.toJson(book);
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path))) {
            bw.write(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Book deserialize(String path) {
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String s;
            s = br.readLine();
            Gson gson = new Gson();
            return gson.fromJson(s, Book.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
