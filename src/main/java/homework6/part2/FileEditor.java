package homework6.part2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileEditor {

    public static boolean createFile(String path) {
        File newFile = new File(path);
        try {
            return newFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean deleteFile(String path) throws FileNotFoundException {
        File newFile = new File(path);
        if (newFile.isFile()) {
            return newFile.delete();
        } else {
            throw new FileNotFoundException("No such file");
        }
    }

    public static boolean renameFile(String path, String path2) throws FileNotFoundException {
        File newFile = new File(path);
        File newFile2 = new File(path2);
        if (newFile.isFile()) {
            return newFile.renameTo(newFile2);
        } else {
            throw new FileNotFoundException("No such file");
        }
    }

    public static boolean listFile(String path) throws FileNotFoundException {
        File newFile = new File(path);
        if (newFile.isDirectory()) {
            File[] files = newFile.listFiles();
            for (int i = 0; i < files.length; i++) {
                System.out.println(files[i]);
            }
            return true;
        } else {
            throw new FileNotFoundException("No such directory");
        }
    }
}
