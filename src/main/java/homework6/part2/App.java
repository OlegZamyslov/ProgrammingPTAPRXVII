package homework6.part2;

import java.io.*;


public class App {
    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        boolean result = false;
        String res;
        while (true) {
            try {
                System.out.println("Input file and command \n 0-for exit");
                res = br.readLine();
                String[] answer = res.split(" ");
                if (answer.length > 1 && !res.equals("0")) {
                    switch (answer[1]) {
                        case "create": {
                            result = FileEditor.createFile(answer[0]);
                            break;
                        }
                        case "rename": {
                            String path2;
                            while (true) {
                                try {
                                    System.out.println("Enter the new file name \n 0-for exit");
                                    path2 = br.readLine();
                                    result = FileEditor.renameFile(answer[0], path2);
                                    break;
                                } catch (IOException e) {
                                    System.out.println("Wrong command!");
                                }
                            }
                            break;
                        }
                        case "delete": {
                            result = FileEditor.deleteFile(answer[0]);
                            break;
                        }
                        case "list": {
                            result = FileEditor.listFile(answer[0]);
                            break;
                        }
                    }
                }
                if (result) {
                    try {
                        System.out.println("Operation completed. \n continue? (0 - exit)");
                        res = br.readLine();
                        if (res.equals("0")) {
                            break;
                        }
                    } catch (IOException e) {
                        System.out.println("Wrong command!");
                    }
                } else if (res.equals("0")) {
                    break;
                }
            } catch (FileNotFoundException e) {
                System.out.println("No such file!");
            } catch (IOException e) {
                System.out.println("Wrong command!");
            }
        }
    }
}
