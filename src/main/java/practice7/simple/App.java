package practice7.simple;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class App {
    private final static String URL = "jdbc:mysql://localhost:3306/practice_database?useSSL=false";
    private final static String USER = "root";
    private final static String PASSWORD = "root";
    private final static String SQL_CREATE_ROLE = "CREATE TABLE IF NOT EXISTS role(id int AUTO_INCREMENT primary key, " +
            "name VARCHAR(10))";
    private final static String SQL_CREATE_ADDRESS = "CREATE TABLE IF NOT EXISTS address(id int AUTO_INCREMENT primary key, " +
            "name VARCHAR(100))";
    private final static String SQL_CREATE_MUSICTYPE = "CREATE TABLE IF NOT EXISTS musictype(id int AUTO_INCREMENT primary key, " +
            "name VARCHAR(10))";
    private final static String SQL_CREATE_USER_MUSICTYPE = "CREATE TABLE IF NOT EXISTS user_musictypes(id int AUTO_INCREMENT primary key, " +
            "user_id int, FOREIGN KEY (user_id) REFERENCES user(id)," +
            "musictype_id int, FOREIGN KEY (musictype_id) REFERENCES musictype(id))";
    private final static String SQL_CREATE_USER = "CREATE TABLE IF NOT EXISTS user(id int AUTO_INCREMENT primary key, " +
            "login VARCHAR(20),pass VARCHAR(20),role_id int, FOREIGN KEY (role_id) REFERENCES role(id)," +
            "address_id int, FOREIGN KEY (address_id) REFERENCES address(id))";

    public static void main(String[] args) {
        try(Connection connection = DriverManager.getConnection(URL,USER, PASSWORD);
            Statement statement = connection.createStatement()){
            statement.execute(SQL_CREATE_ROLE);
            statement.execute(SQL_CREATE_ADDRESS);
            statement.execute(SQL_CREATE_MUSICTYPE);
            statement.execute(SQL_CREATE_USER);
            statement.execute(SQL_CREATE_USER_MUSICTYPE);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
