package practice7.model;

import lombok.*;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Role extends Entity<Integer>{
    String name;
    Set<User> users = new HashSet<>();

    public Role(String name) {
        this.name = name;
    }
}
