package practice7.model;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Address extends Entity<Integer>{
    String address;
    User user;

    public Address(String address) {
        this.address = address;
    }
}
