package practice7.model;

import lombok.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class User extends Entity<Integer>{
    String login;
    String pass;
    Role role;
    Address address;
    Set<MusicType> types = new HashSet<>();

    public User(String login, String pass, Role role, Address address) {
        this.login = login;
        this.pass = pass;
        this.role = role;
        this.address = address;
    }
}
