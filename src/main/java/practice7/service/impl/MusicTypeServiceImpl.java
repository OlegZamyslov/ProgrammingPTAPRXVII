package practice7.service.impl;

import practice7.dao.DaoFactory;
import practice7.dao.api.Dao;
import practice7.dto.MusicTypeDTO;
import practice7.mapper.BeanMapper;
import practice7.model.MusicType;
import practice7.service.api.Service;

import java.util.List;


public class MusicTypeServiceImpl implements Service<Integer, MusicTypeDTO> {

    private static MusicTypeServiceImpl service;
    private Dao<Integer, MusicType> musicTypeDao;
    private BeanMapper beanMapper;

    private MusicTypeServiceImpl() {
        musicTypeDao = DaoFactory.getInstance().getMusicTypeDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized MusicTypeServiceImpl getInstance() {
        if (service == null) {
            service = new MusicTypeServiceImpl();
        }
        return service;
    }


    @Override
    public List<MusicTypeDTO> getAll() {
        List<MusicType> musicTypes = musicTypeDao.getAll();
        List<MusicTypeDTO> musicTypeDTOs = beanMapper.listMapToList(musicTypes, MusicTypeDTO.class);
        return musicTypeDTOs;
    }

    @Override
    public void save(MusicTypeDTO musicTypeDto) {
        MusicType musicType = beanMapper.singleMapper(musicTypeDto, MusicType.class);
        musicTypeDao.save(musicType);
    }

    @Override
    public MusicTypeDTO getById(Integer id) {
        MusicType musicType = musicTypeDao.getById(id);
        MusicTypeDTO musicTypeDTO = beanMapper.singleMapper(musicType, MusicTypeDTO.class);
        return musicTypeDTO;
    }

    public MusicTypeDTO getByName(String name) {
        MusicType musicType = musicTypeDao.getBy("name",name);
        MusicTypeDTO musicTypeDTO = beanMapper.singleMapper(musicType, MusicTypeDTO.class);
        return musicTypeDTO;
    }

    @Override
    public void delete(Integer id) {
        musicTypeDao.delete(id);
    }

    @Override
    public void update(MusicTypeDTO entity) {
        MusicType musicType = beanMapper.singleMapper(entity, MusicType.class);
        musicTypeDao.update(musicType);
    }

}
