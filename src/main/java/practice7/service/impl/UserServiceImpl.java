package practice7.service.impl;

import practice7.dao.DaoFactory;
import practice7.dao.api.Dao;
import practice7.dto.UserDTO;
import practice7.mapper.BeanMapper;
import practice7.model.User;
import practice7.service.api.Service;

import java.util.List;


public class UserServiceImpl implements Service<Integer, UserDTO> {

    private static UserServiceImpl service;
    private Dao<Integer, User> userDao;
    private BeanMapper beanMapper;

    private UserServiceImpl() {
        userDao = DaoFactory.getInstance().getUserDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized UserServiceImpl getInstance() {
        if (service == null) {
            service = new UserServiceImpl();
        }
        return service;
    }


    @Override
    public List<UserDTO> getAll() {
        List<User> users = userDao.getAll();
        List<UserDTO> UserDTOs = beanMapper.listMapToList(users, UserDTO.class);
        return UserDTOs;
    }

    @Override
    public void save(UserDTO userDto) {
        User user = beanMapper.singleMapper(userDto, User.class);
        userDao.save(user);
    }

    @Override
    public UserDTO getById(Integer id) {
        User movie = userDao.getById(id);
        UserDTO UserDTO = beanMapper.singleMapper(movie, UserDTO.class);
        return UserDTO;
    }

    public UserDTO getByLogin(String login) {
        User movie = userDao.getBy("login",login);
        UserDTO UserDTO = beanMapper.singleMapper(movie, UserDTO.class);
        return UserDTO;
    }

    @Override
    public void delete(Integer id) {
        userDao.delete(id);
    }

    @Override
    public void update(UserDTO entity) {
        User user = beanMapper.singleMapper(entity, User.class);
        userDao.update(user);
    }

}
