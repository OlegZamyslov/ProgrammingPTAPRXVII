package practice7.service.impl;

import practice7.dao.DaoFactory;
import practice7.dao.api.Dao;
import practice7.dto.AddressDTO;
import practice7.mapper.BeanMapper;
import practice7.model.Address;
import practice7.service.api.Service;

import java.util.List;


public class AddressServiceImpl implements Service<Integer, AddressDTO> {

    private static AddressServiceImpl service;
    private Dao<Integer, Address> addressDao;
    private BeanMapper beanMapper;

    private AddressServiceImpl() {
        addressDao = DaoFactory.getInstance().getAddressDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized AddressServiceImpl getInstance() {
        if (service == null) {
            service = new AddressServiceImpl();
        }
        return service;
    }


    @Override
    public List<AddressDTO> getAll() {
        List<Address> addresss = addressDao.getAll();
        List<AddressDTO> addressDTOs = beanMapper.listMapToList(addresss, AddressDTO.class);
        return addressDTOs;
    }

    @Override
    public void save(AddressDTO addressDto) {
        Address address = beanMapper.singleMapper(addressDto, Address.class);
        addressDao.save(address);
    }

    @Override
    public AddressDTO getById(Integer id) {
        Address address = addressDao.getById(id);
        AddressDTO addressDTO = beanMapper.singleMapper(address, AddressDTO.class);
        return addressDTO;
    }

    public AddressDTO getByName(String name) {
        Address address = addressDao.getBy("name",name);
        AddressDTO addressDTO = beanMapper.singleMapper(address, AddressDTO.class);
        return addressDTO;
    }

    @Override
    public void delete(Integer id) {
        addressDao.delete(id);
    }

    @Override
    public void update(AddressDTO entity) {
        Address address = beanMapper.singleMapper(entity, Address.class);
        addressDao.update(address);
    }

}
