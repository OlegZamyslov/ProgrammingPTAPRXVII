package practice7.dto;

import lombok.*;
import practice7.model.Entity;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class RoleDTO extends Entity<Integer> {
    String name;

}
