package practice7.dto;

import lombok.*;
import practice7.model.Entity;

import java.util.Set;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class MusicTypeDTO extends Entity<Integer> {
    String name;
    Set<UserDTO> users;
}
