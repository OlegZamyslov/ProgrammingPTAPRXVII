package practice7.dto;

import lombok.*;
import practice7.model.Entity;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class AddressDTO extends Entity<Integer>{
    String adress;
}
