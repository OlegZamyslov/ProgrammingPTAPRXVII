package practice7.dto;

import lombok.*;
import practice7.model.Entity;
import java.util.Set;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class UserDTO extends Entity<Integer> {
    String login;
    String pass;
    RoleDTO role;
    AddressDTO address;
    Set<MusicTypeDTO> types;
}
