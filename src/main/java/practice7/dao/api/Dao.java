package practice7.dao.api;




import practice7.model.Entity;

import java.util.List;

/**
 * Created by Kovantonlenko on 4/5/2016.
 */
public interface Dao<K, T extends Entity<K>> {

    List<T> getAll();

    T getById(K key);

    T getBy(String fieldName, String key);

    void save(T entity);

    void delete(K key);

    void update(T entity);

}
