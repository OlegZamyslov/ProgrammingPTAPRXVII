package practice7.dao;


import practice7.dao.api.Dao;
import practice7.dao.impl.AddressDaoImpl;
import practice7.dao.impl.MusicTypeDaoImpl;
import practice7.dao.impl.RoleDaoImpl;
import practice7.dao.impl.UserDaoImpl;
import practice7.helper.PropertyHolder;
import practice7.model.Address;
import practice7.model.MusicType;
import practice7.model.Role;
import practice7.model.User;
import practice7.service.impl.MusicTypeServiceImpl;

public class DaoFactory {
    private static DaoFactory instance = null;

    private Dao<Integer, User> userDao;
    private Dao<Integer, Role> roleDao;
    private Dao<Integer, Address> addressDao;
    private Dao<Integer, MusicType> musicTypeDao;

    private DaoFactory() {
        loadDaos();
    }

    public static DaoFactory getInstance() {
        if (instance == null) {
            instance = new DaoFactory();
        }
        return instance;
    }

    private void loadDaos() {
        if (PropertyHolder.getInstance().isInMemoryDB()) {

        } else {
            userDao = UserDaoImpl.getInstance();
            roleDao = RoleDaoImpl.getInstance();
            addressDao = AddressDaoImpl.getInstance();
            musicTypeDao = MusicTypeDaoImpl.getInstance();
        }
    }

    public Dao<Integer, User> getUserDao() {
        return userDao;
    }

    public void setUserDao(Dao<Integer, User> userDao) {
        this.userDao = userDao;
    }

    public Dao<Integer, Role> getRoleDao() {
        return roleDao;
    }

    public void setRoleDao(Dao<Integer, Role> roleDao) {
        this.roleDao = roleDao;
    }

    public Dao<Integer, Address> getAddressDao() {
        return addressDao;
    }

    public void setAddressDao(Dao<Integer, Address> adressDao) {
        this.addressDao = adressDao;
    }

    public Dao<Integer, MusicType> getMusicTypeDao() {
        return musicTypeDao;
    }

    public void setMusicTypeDao(Dao<Integer, MusicType> musicTypeDao) {
        this.musicTypeDao = musicTypeDao;
    }
}
