package practice7.dao.impl;

import practice7.datasource.DataSource;
import practice7.model.Address;
import practice7.model.MusicType;
import practice7.model.Role;
import practice7.model.User;

import java.sql.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


public final class UserDaoImpl extends CrudDAO<User> {
    private final String INSERT = "Insert into USER (login, pass, role_id, address_id) values (?,?,?,?)";
    private final String UPDATE = "UPDATE USER SET login = ?, pass = ?, role_id = ?, address_id = ? WHERE id = ?";
    private final String SELECT_USER_MUSICTYPES = "Select musictype_id from USER_MUSICTYPES WHERE user_id = ?";
    private final String INSERT_USER_MUSICTYPES = "Insert into USER_MUSICTYPES (user_id, musictype_id) values (?,?)";
    private final String DELETE_USER_MUSICTYPES = "DELETE FROM USER_MUSICTYPES WHERE %s = ?";
    private final String DELETE_USER = "DELETE FROM USER WHERE id = ?";
    private static UserDaoImpl crudDAO;

    private UserDaoImpl(Class type) {
        super(type);
    }


    public static synchronized UserDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new UserDaoImpl(User.class);
        }
        return crudDAO;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, User entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, entity.getLogin());
        preparedStatement.setString(2, entity.getPass());
        preparedStatement.setInt(3, entity.getRole().getId());
        preparedStatement.setInt(4, entity.getAddress().getId());
        preparedStatement.setInt(5, entity.getId());
        return preparedStatement;
    }

    @Override
    public PreparedStatement createInsertStatement(Connection connection, User entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getLogin());
        preparedStatement.setString(2, entity.getPass());
        preparedStatement.setInt(3, entity.getRole().getId());
        preparedStatement.setInt(4, entity.getAddress().getId());
        return preparedStatement;
    }

    public void createInsertStatementMT(Connection connection, User entity) throws SQLException {
        PreparedStatement preparedStatementMT = connection.prepareStatement(INSERT_USER_MUSICTYPES, Statement.RETURN_GENERATED_KEYS);
        for (MusicType mt : entity.getTypes()) {
            preparedStatementMT.setInt(1, entity.getId());
            preparedStatementMT.setInt(2, mt.getId());
            preparedStatementMT.executeUpdate();
        }
    }

    @Override
    public void save(User entity) {
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = createInsertStatement(connection, entity)) {
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                entity.setId(rs.getInt(1));
            }
            createInsertStatementMT(connection, entity);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Set<MusicType> getUserMusicTypes(User entity) throws SQLException {
        Set<MusicType> types = new HashSet<>();
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_MUSICTYPES)) {
            preparedStatement.setInt(1, entity.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            MusicType mt;
            while (resultSet.next()) {
                mt = MusicTypeDaoImpl.getInstance().getById(resultSet.getInt("musictype_id"));
                types.add(mt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return types;
    }

    @Override
    public void update(User entity) {
        String sql = String.format(DELETE_USER_MUSICTYPES, "user_id");
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = createUpdateStatement(connection, entity);
             PreparedStatement preparedStatementDel = connection.prepareStatement(sql)) {
            preparedStatementDel.setInt(1, entity.getId());
            preparedStatementDel.executeUpdate();
            preparedStatement.executeUpdate();
            createInsertStatementMT(connection, entity);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteUserMusicType(Integer key, String field) {
        String sql = String.format(DELETE_USER_MUSICTYPES, field);

        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatementDel = connection.prepareStatement(sql)) {
            preparedStatementDel.setInt(1, key);
            preparedStatementDel.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer key) {
        try (Connection connection = DataSource.getInstance().getConnection()) {
            deleteUserMusicType(key, "user_id");
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER);
            preparedStatement.setInt(1, key);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public List<User> readAll(ResultSet resultSet) throws SQLException {
        List<User> result = new LinkedList<>();
        User user;
        while (resultSet.next()) {
            user = new User();
            user.setId(resultSet.getInt("id"));
            user.setLogin(resultSet.getString("login"));
            user.setPass(resultSet.getString("pass"));
            Role role = RoleDaoImpl.getInstance().getById(resultSet.getInt("role_id"));
            Address address = AddressDaoImpl.getInstance().getById(resultSet.getInt("address_id"));
            user.setAddress(address);
            user.setRole(role);
            user.setTypes(getUserMusicTypes(user));
            result.add(user);
        }
        return result;
    }

}