package practice7.dao.impl;

import practice7.model.Role;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;


public class RoleDaoImpl extends CrudDAO<Role> {
    private final String INSERT = "Insert into ROLE (name) values (?)";
    private final String UPDATE = "UPDATE ROLE SET name = ? WHERE id = ?";
    private static RoleDaoImpl crudDAO;

    private RoleDaoImpl(Class type) {
        super(type);
    }


    public static synchronized RoleDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new RoleDaoImpl(Role.class);
        }
        return crudDAO;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Role entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setInt(2, entity.getId());
        return preparedStatement;
    }

    @Override
    public PreparedStatement createInsertStatement(Connection connection, Role entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getName());
        return preparedStatement;
    }

    @Override
    public List<Role> readAll(ResultSet resultSet) throws SQLException {
        List<Role> result = new LinkedList<>();
        Role role;
        while (resultSet.next()) {
            role = new Role();
            role.setId(resultSet.getInt("id"));
            role.setName(resultSet.getString("name"));
            result.add(role);
        }
        return result;
    }

}
