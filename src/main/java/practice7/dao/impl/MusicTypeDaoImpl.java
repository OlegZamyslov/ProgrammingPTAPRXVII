package practice7.dao.impl;

import practice7.datasource.DataSource;
import practice7.model.MusicType;
import practice7.model.User;

import java.sql.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


public class MusicTypeDaoImpl extends CrudDAO<MusicType> {
    private final String INSERT = "Insert into musictype (name) values (?)";
    private final String UPDATE = "UPDATE musictype SET name = ? WHERE id = ?";
    private final String DELETE_MUSICTYPE = "DELETE FROM musictype WHERE user_id = ?";
    private final String SELECT_USER_MUSICTYPES = "Select user_id from USER_MUSICTYPES WHERE musictype_id = ?";
    private static MusicTypeDaoImpl crudDAO;

    private MusicTypeDaoImpl(Class type) {
        super(type);
    }


    public static synchronized MusicTypeDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new MusicTypeDaoImpl(MusicType.class);
        }
        return crudDAO;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, MusicType entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setInt(2, entity.getId());
        return preparedStatement;
    }

    @Override
    public PreparedStatement createInsertStatement(Connection connection, MusicType entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getName());
        return preparedStatement;
    }

    @Override
    public void delete(Integer key) {
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_MUSICTYPE)) {
            UserDaoImpl.getInstance().deleteUserMusicType(key, "musictype_id");
            preparedStatement.setInt(1, key);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Set<User> getUserMusicTypes(MusicType entity) throws SQLException {
        Set<User> types = new HashSet<>();
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_MUSICTYPES)) {
            preparedStatement.setInt(1, entity.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            User user;
            while (resultSet.next()) {
                user = UserDaoImpl.getInstance().getById(resultSet.getInt("user_id"));
                types.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return types;
    }

    @Override
    public List<MusicType> readAll(ResultSet resultSet) throws SQLException {
        List<MusicType> result = new LinkedList<>();
        MusicType musicType;
        while (resultSet.next()) {
            musicType = new MusicType();
            musicType.setId(resultSet.getInt("id"));
            musicType.setName(resultSet.getString("name"));
            //Set<User> users = getUserMusicTypes(musicType);
            //musicType.setUsers(users);
            result.add(musicType);
        }
        return result;
    }

}
