package practice7.dao.impl;

import practice7.model.Address;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;


public class AddressDaoImpl extends CrudDAO<Address> {
    private final String INSERT = "Insert into address (name) values (?)";
    private final String UPDATE = "UPDATE ROLE SET name = ?,user_id = ? WHERE id = ?";
    private static AddressDaoImpl crudDAO;

    private AddressDaoImpl(Class type) {
        super(type);
    }


    public static synchronized AddressDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new AddressDaoImpl(Address.class);
        }
        return crudDAO;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Address entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, entity.getAddress());
        preparedStatement.setInt(2, entity.getUser().getId());
        preparedStatement.setInt(3, entity.getId());
        return preparedStatement;
    }

    @Override
    public PreparedStatement createInsertStatement(Connection connection, Address entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getAddress());
        return preparedStatement;
    }

    @Override
    public List<Address> readAll(ResultSet resultSet) throws SQLException {
        List<Address> result = new LinkedList<>();
        Address address = null;
        while (resultSet.next()) {
            address = new Address();
            address.setId(resultSet.getInt("id"));
            address.setAddress(resultSet.getString("name"));
            result.add(address);
        }
        return result;
    }

}
