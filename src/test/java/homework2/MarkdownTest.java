package homework2;

import org.junit.Test;

import static org.junit.Assert.*;


public class MarkdownTest {
    @Test
    public void parser() throws Exception {
        String excepted = "<html>\n" +
                "<body>\n" +
                "<h2>Header line</h2>\n" +
                "<h3>Header line</h3>\n" +
                "<p>Simple line <em>with</em> em</p>\n" +
                "<p>Simple <strong>line</strong> <em>with</em> with strong</p>\n" +
                "<p>Simple <strong>line</strong> with strong</p>\n" +
                "<p>Line with link <a href=“https://www.google.com“>Link to google</a> in center</p>\n" +
                "<p>Line <strong>with</strong> <em>many</em> <strong>elements</strong> and link <a href=“https://www.facebook.com“>Link to FB</a></p>\n" +
                "</body>\n" +
                "</html>\n";
        String actual = Markdown.parser("##Header line\n" +
                "###Header line\n" +
                "Simple line *with* em\n" +
                "Simple **line** *with* with strong\n" +
                "Simple **line** with strong\n" +
                "Line with link [Link to google](https://www.google.com) in center\n" +
                "Line **with** *many* **elements** and link [Link to FB](https://www.facebook.com)\n");
        assertEquals(excepted, actual);


    }
    @Test
    public void parser2() throws Exception {
        String excepted = "<html>\n<body>\n<p><em>with</em> em</p>\n</body>\n</html>\n";
        String actual = Markdown.parser("*with* em\n");
        assertEquals(excepted, actual);
    }
    @Test
    public void parser3() throws Exception {
        String excepted = "<html>\n<body>\n<p>Line <em>with</em>no<em>spaces</em></p>\n</body>\n</html>\n";
        String actual = Markdown.parser("Line *with*no*spaces*\n");
        assertEquals(excepted, actual);
    }
    @Test
    public void parser4() throws Exception {
        String excepted = "<html>\n<body>\n<p><strong>with</strong> em</p>\n</body>\n</html>\n";
        String actual = Markdown.parser("**with** em\n");
        assertEquals(excepted, actual);
    }
}