package homework2;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class StringUtilsTest {
    @Test
    public void reverseString() {
        String excepted = "!dlrow olleH";
        String actual = StringUtils.reverseString("Hello world!");
        assertEquals(excepted, actual);
    }

    @Test
    public void isPalindromeString() {
        assertTrue(StringUtils.isPalindromeString("А роза-упала на лапу Азора"));
    }

    @Test
    public void isPalindromeString2() {
        assertFalse(StringUtils.isPalindromeString("А роза-упла на лапу Азора"));
    }

    @Test
    public void checkStringLength() {
        String excepted = "the la";
        String actual = StringUtils.checkStringLength("the large string");
        assertEquals(excepted, actual);
    }

    @Test
    public void checkStringLength2() {
        String excepted = "smallooooooo";
        String actual = StringUtils.checkStringLength("small");
        assertEquals(excepted, actual);
    }

    @Test
    public void changeFirstLastWord() throws Exception {
        String excepted = "last, word word first!";
        String actual = StringUtils.changeFirstLastWord("first, word word last!");
        assertEquals(excepted, actual);
    }

    @Test
    public void changeFirstLastWord2() throws Exception {
        String excepted = "firstwordwordlast";
        String actual = StringUtils.changeFirstLastWord("firstwordwordlast");
        assertEquals(excepted, actual);
    }
    @Test
    public void changeFirstLastWord3() throws Exception {
        String excepted = "слово слово, второе первое!";
        String actual = StringUtils.changeFirstLastWord("первое слово, второе слово!");
        assertEquals(excepted, actual);
    }

    @Test
    public void changeFirstLastWordEverywhere() throws Exception {
        String excepted = "";
        String actual = StringUtils.changeFirstLastWordEverywhere("firstwordwordlast");
        assertEquals(excepted, actual);
    }

    @Test
    public void changeFirstLastWordEverywhere2() throws Exception {
        String excepted = "word first.last word.";
        String actual = StringUtils.changeFirstLastWordEverywhere("first word.word last.");
        assertEquals(excepted, actual);
    }

    @Test
    public void changeFirstLastWordEverywhere3() throws Exception {
        String excepted = "world, my Hello.world, your Bye.";
        String actual = StringUtils.changeFirstLastWordEverywhere("Hello, my world.Bye, your world.");
        assertEquals(excepted, actual);
    }

    @Test
    public void containsABC() throws Exception {
        assertTrue(StringUtils.containsABC("abcbcbcba"));
    }

    @Test
    public void containsABC2() throws Exception {
        assertFalse(StringUtils.containsABC("first word.word last."));
    }

    @Test
    public void containsDate() throws Exception {
        assertTrue(StringUtils.containsDate("20.02.2017"));
    }

    @Test
    public void containsDate2() throws Exception {
        assertFalse(StringUtils.containsDate("20.02.17"));
    }

    @Test
    public void containsEmail() throws Exception {
        assertTrue(StringUtils.containsEmail("Vasya@gmail.com"));
    }

    @Test
    public void containsEmail2() throws Exception {
        assertTrue(StringUtils.containsEmail("Vasya.Pupkin@gmail.com"));
    }

    @Test
    public void containsEmail3() throws Exception {
        assertFalse(StringUtils.containsEmail("Vasya@gm ail.com"));
    }

    @Test
    public void getTelNumbers() throws Exception {
        List<String> excepted = new ArrayList<>();
        excepted.add("+3(095)111-22-33");
        excepted.add("+3(095)111-22-34");
        List<String> actual = StringUtils.getPhoneNumbers(" tel +3(095)111-22-33, tel2 +3(095)111-22-34 tel3 +3(095)111-22-359 wrong +3(05)111-22-35");
        assertEquals(excepted, actual);
    }

}