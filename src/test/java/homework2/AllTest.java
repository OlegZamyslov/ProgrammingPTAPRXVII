package homework2;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({MarkdownTest.class,StringUtilsTest.class})
public class AllTest {
}
