package practice1.student;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;


public class StudentTest {
    ArrayList<Student> students;
    @Before
    public void setUp() throws Exception {
        students = new ArrayList<>();
        Group group = new Group(1,"Programming");
        Student student1 = new Student("Vasya","Orlov",group);
        student1.getListExam().add(new Exam(ExamNames.ALGEBRA,0,1,2017));
        student1.getListExam().add(new Exam(ExamNames.GEOMETRY,0,1,2017));
        student1.getListExam().add(new Exam(ExamNames.LITERATURE,0,2,2017));
        student1.addMark(5,ExamNames.ALGEBRA,1,2017);
        student1.addMark(4,ExamNames.GEOMETRY,1,2017);
        student1.addMark(5,ExamNames.LITERATURE,1,2017);
        students.add(student1);
        Student student2 = new Student("Petya","Sidorov",group);
        student2.getListExam().add(new Exam(ExamNames.ALGEBRA,0,1,2017));
        student2.getListExam().add(new Exam(ExamNames.GEOMETRY,0,1,2017));
        student2.getListExam().add(new Exam(ExamNames.LITERATURE,0,2,2017));
        student2.getListExam().add(new Exam(ExamNames.PHYSICS,0,2,2017));
        student2.addMark(5,ExamNames.ALGEBRA,1,2017);
        student2.addMark(5,ExamNames.GEOMETRY,1,2017);
        student2.addMark(2,ExamNames.LITERATURE,2,2017);
        student2.addMark(3,ExamNames.PHYSICS,2,2017);
        students.add(student2);
    }

    @After
    public void tearDown() throws Exception {
        students =null;
    }

    @Test
    public void addMark() throws Exception {
        assertTrue(students.get(0).addMark(5,ExamNames.ALGEBRA,1,2017));
    }

    @Test
    public void addMark2() throws Exception {
        assertFalse(students.get(0).addMark(5,ExamNames.ALGEBRA,1,2016));
    }

    @Test
    public void deleteMark() throws Exception {
        assertTrue(students.get(0).deleteMark(ExamNames.ALGEBRA,1,2017));
    }

    @Test(expected = NoSuchExamException.class)
    public void deleteMark2() throws NoSuchExamException {
        students.get(0).deleteMark(ExamNames.ALGEBRA,1,2016);
    }

    @Test
    public void numberExam() throws Exception {
        assertEquals(2,students.get(1).numberExam(5));
    }

    @Test
    public void averageMark() throws Exception {
        assertEquals(2.5,students.get(1).averageMark(2,2017),0);
    }

}