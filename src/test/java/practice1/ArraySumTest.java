package practice1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySumTest {
    int[]  myArray;

    @Before
    public void setUp() throws Exception {
        myArray = new int[] {5,4,2,1,6};
    }


    @Test
    public void staticSum() throws Exception {
        assertEquals(18, ArraySum.sum(myArray));
    }

    @Test
    public void sum() throws Exception {
        assertEquals(18, new ArraySum(myArray).sum());
    }

    @Test(expected = NullPointerException.class)
    public void sumError() throws Exception {
        ArraySum.sum(null);
    }


}