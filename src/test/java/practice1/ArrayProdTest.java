package practice1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class ArrayProdTest {
    int[]  myArray;

    @Before
    public void setUp() throws Exception {
        myArray = new int[] {5,4,2,1,6};
    }


    @Test
    public void staticSum() throws Exception {
        assertEquals(240, ArrayProd.prod(myArray));
    }

    @Test
    public void sum() throws Exception {
        assertEquals(240, new ArrayProd(myArray).prod());
    }

    @Test(expected = NullPointerException.class)
    public void sumError() throws Exception {
        ArrayProd.prod(null);
    }

}