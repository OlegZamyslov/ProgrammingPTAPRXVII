package homework3;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({GenericStorageStringTest.class,GenericStorageIntegerTest.class})
public class AllTest {
}
