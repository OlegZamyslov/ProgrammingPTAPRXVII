package homework3;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;


public class GenericStorageStringTest {
    GenericStorage<String> myStringStorage;

    @Before
    public void setUp() {
        myStringStorage = new GenericStorage<>();
        myStringStorage.add("Vasya");
        myStringStorage.add("Petya");
        myStringStorage.add("Marina");
    }

    @After
    public void tearDown() {
        myStringStorage = null;
    }

    @Test(expected = IllegalArgumentException.class)
    public void create() throws IllegalArgumentException {
        myStringStorage = new GenericStorage<>(-1);
    }

    @Test
    public void add() throws Exception {
        assertTrue(myStringStorage.add("Maxim"));
    }

    @Test(expected = NullPointerException.class)
    public void add2() throws NullPointerException {
        myStringStorage.add(null);
    }

    @Test
    public void get() throws Exception {
        String excepted = "Petya";
        String actual = myStringStorage.get(1);
        assertEquals(excepted, actual);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void get2() throws ArrayIndexOutOfBoundsException {
        myStringStorage.get(5);
    }

    @Test
    public void getAll() throws Exception {
        Object[] excepted = new String[]{"Vasya", "Petya", "Marina"};
        Object[] actual = myStringStorage.getAll();
        assertTrue("Arrays are not equal", java.util.Arrays.equals(excepted, actual));

    }

    @Test
    public void update() throws Exception {
        String excepted = "Oksana";
        myStringStorage.update(1, "Oksana");
        String actual = myStringStorage.get(1);
        assertEquals(excepted, actual);
    }

    @Test
    public void delete() throws Exception {
        String excepted = "Vasya";
        myStringStorage.delete(1);
        String actual = myStringStorage.get(0);
        assertEquals(excepted, actual);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void delete1() throws ArrayIndexOutOfBoundsException {
        myStringStorage.delete("Marina");
        myStringStorage.get(2);
    }

    @Test
    public void size() throws Exception {
        int excepted = 3;
        int actual = myStringStorage.size();
        assertEquals(excepted, actual);
    }

}