package homework3;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class GenericStorageIntegerTest {

    GenericStorage<Integer> myIntegerStorage;

    @Before
    public void setUp() throws Exception {
        myIntegerStorage = new GenericStorage<>(5);
        myIntegerStorage.add(1);
        myIntegerStorage.add(2);
    }

    @After
    public void tearDown() throws Exception {
        myIntegerStorage = null;
    }

    @Test(expected = IllegalArgumentException.class)
    public void create() throws IllegalArgumentException {
        myIntegerStorage = new GenericStorage<>(-1);
    }

    @Test
    public void add() throws Exception {
        assertTrue(myIntegerStorage.add(1));
    }

    @Test(expected = NullPointerException.class)
    public void add2() throws NullPointerException {
        myIntegerStorage.add(null);
    }

    @Test
    public void get() throws Exception {
        Integer excepted = 2;
        Integer actual = myIntegerStorage.get(1);
        assertEquals(excepted, actual);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void get2() throws ArrayIndexOutOfBoundsException {
        myIntegerStorage.get(2);
    }

    @Test
    public void getAll() throws Exception {
        Object[] excepted = new Integer[]{1, 2};
        Object[] actual = myIntegerStorage.getAll();
        assertTrue("Arrays are not equal", java.util.Arrays.equals(excepted, actual));
    }

    @Test
    public void update() throws Exception {
        Integer excepted = 3;
        myIntegerStorage.update(1, new Integer(3));
        Integer actual = myIntegerStorage.get(1);
        assertEquals(excepted, actual);
    }

    @Test
    public void delete() throws Exception {
        Integer excepted = 2;
        myIntegerStorage.delete(0);
        Integer actual = myIntegerStorage.get(0);
        assertEquals(excepted, actual);

    }

    @Test
    public void delete1() throws Exception {
        Integer excepted = 2;
        myIntegerStorage.delete(new Integer(1));
        Integer actual = myIntegerStorage.get(0);
        assertEquals(excepted, actual);
    }

    @Test
    public void size() throws Exception {
        int excepted = 2;
        int actual = myIntegerStorage.size();
        assertEquals(excepted, actual);
    }

}